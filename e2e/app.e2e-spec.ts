import { WprestNgrxPage } from './app.po';

describe('wprest-ngrx App', () => {
  let page: WprestNgrxPage;

  beforeEach(() => {
    page = new WprestNgrxPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
