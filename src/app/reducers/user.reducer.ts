import * as user from '../actions/user.actions';

export interface State {
    loggedIn: boolean;
    user?: string;
}

const initialState: State = {
    loggedIn: false,
};

export function reducer(state = initialState, action: user.Actions ): State {
    switch (action.type) {
        case user.LOG_IN:
            return Object.assign({}, state, {
                loggedIn: true,
            });

        case user.LOG_OUT:
            return Object.assign({}, state, {
                loggedIn: false
            });

        case user.LOG_IN_AS:
            return Object.assign({}, state, {
                loggedIn: true,
                user: action.payload
            });

        case user.LOG_IN_SUCCESS:
            return Object.assign({}, state, {
            loggedIn: true,
            user: action.payload
        });

        case user.AUTH_VALID:
            return Object.assign({}, state, {
                loggedIn: true,
                user: action.payload
            });

        case user.AUTH_INVALID:
            return Object.assign({}, state, {
                loggedIn: false,
                user: {}
            });

        default:
            return state;

    }
}

export const isUserLoggedIn = (state: State) => state.loggedIn;