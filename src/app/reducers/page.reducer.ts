import * as page from '../actions/page.actions';

export interface State {
    page?: any;
    loading: boolean;
}

const initialState: State = {
    loading: true,
};

export function reducer(state = initialState, action: page.Actions ): State {
    switch (action.type) {
        case page.LOAD_PAGE:
            return Object.assign({}, state, {
                loading: true,
            });

        case page.PAGE_LOADED:
            return Object.assign({}, state, {
                loading: false,
                page: action.payload
            });

        default:
            return state;

    }
}

export const currentPageState = (state: State) => state.page;
export const getPageLoadingStatus = (state: State) => state.loading;