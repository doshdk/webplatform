import { Routes } from '@angular/router';
import { PageComponent } from './components/page/page.component';
import { AdminComponent } from './components/admin/admin.component';
import { PageResolver } from './resolvers/page.resolver';

export const routes: Routes = [
    {
        path: '',
        component: PageComponent,
        resolve: {
            page: PageResolver
        }
    },
    {
        path: 'admin',
        component: AdminComponent
    },
    {
        path: ':slug',
        component: PageComponent,
        resolve: {
            page: PageResolver
        }
    },
    {
        path: ':parent/:slug',
        component: PageComponent,
        resolve: {
            page: PageResolver
        }
    },
    {
        path: 'admin',
        component: AdminComponent
    }
    /*{
        path: 'book/find',
        component: FindBookPageComponent
    },
    {
        path: 'book/:id',
        canActivate: [ BookExistsGuard ],
        component: ViewBookPageComponent
    },
    {
        path: '**',
        component: NotFoundPageComponent
    }*/
];