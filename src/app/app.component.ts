import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from './reducers';

import * as user from './actions/user.actions';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isLoggedIn: Observable<boolean>;

  constructor(private store: Store<fromRoot.State>) {
    this.isLoggedIn = this.store.select(fromRoot.getLoggedInStatus);
    this.store.dispatch({type: user.GET_LOG_IN_STATUS});
  }

  logIn() {
    this.store.dispatch(new user.LogInAction());
  }

  logInAs() {
    this.store.dispatch(new user.LogInAsAction('12345'));
  }

  logOut() {
    this.store.dispatch(new user.LogOutAction());
  }
}
