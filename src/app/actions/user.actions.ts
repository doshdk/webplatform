import { Action } from '@ngrx/store';

export const LOG_IN =   '[User] Log in';
export const GET_LOG_IN_STATUS = '[User] Login status';
export const AUTH_VALID = '[User] Auth valid';
export const AUTH_INVALID = '[User] Auth invalid';
export const LOG_IN_SUCCESS =   '[User] Login succeeded';
export const LOG_IN_FAIL =   '[User] Log in failed';
export const LOG_OUT =   '[User] Log out';
export const LOGGED_OUT =   '[User] Logged out';
export const LOG_IN_AS = '[User] Log in as';

export class LogInAction implements Action {
    readonly type = LOG_IN;
}

export class GetLoginStatusAction implements Action {
    readonly type = GET_LOG_IN_STATUS;
}

export class LogOutAction implements Action {
    readonly type = LOG_OUT;
}

export class LogInAsAction implements Action {
    readonly type = LOG_IN_AS;

    constructor(public payload: string) { }
}

export class LogInSuccessAction implements Action {
    readonly type = LOG_IN_SUCCESS;

    constructor(public payload: any) { }
}

export class AuthValidAction implements Action {
    readonly type = AUTH_VALID;

    constructor(public payload: any) { }
}

export class AuthInvalidAction implements Action {
    readonly type = AUTH_INVALID;
}

export type Actions
    = LogInAction
    | LogOutAction
    | LogInAsAction
    | LogInSuccessAction
    | GetLoginStatusAction
    | AuthInvalidAction
    | AuthValidAction;