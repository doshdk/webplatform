import { Action } from '@ngrx/store';

export const LOAD_PAGE =   '[Page] Load page';
export const PAGE_LOADED =   '[Page] Page loaded';

export class LoadPageAction implements Action {
    readonly type = LOAD_PAGE;
}

export class PageLoadedAction implements Action {
    readonly type = PAGE_LOADED;

    constructor(public payload: any) { }
}

export type Actions
    = LoadPageAction
    |PageLoadedAction;
