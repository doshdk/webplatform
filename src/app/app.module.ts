import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FroalaEditorModule, FroalaViewModule } from 'angular2-froala-wysiwyg';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { RouterStoreModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { UserEffects } from './effects/user.effects';

import { PageResolver } from './resolvers/page.resolver';

import { routes } from './routes';
import { reducer } from './reducers';

import { WordpressService } from './services/wordpress.service';
import { CacheService } from './services/cache.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { GenericBlockDirective } from './components/page-blocks/generic-block.directive';

import { AppComponent } from './app.component';
import { PageComponent } from './components/page/page.component';
import { AdminComponent } from './components/admin/admin.component';
import { SimpleTextComponent } from './components/page-blocks/simple-text/simple-text.component';
import { ImageGridComponent } from './components/page-blocks/image-grid/image-grid.component';
import { HeaderComponent } from './components/layout/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    GenericBlockDirective,
    PageComponent,
    AdminComponent,
    SimpleTextComponent,
    ImageGridComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    StoreModule.provideStore(reducer),
    RouterModule.forRoot(routes),
    RouterStoreModule.connectRouter(),
    StoreDevtoolsModule.instrumentOnlyWithExtension(),
    EffectsModule.run(UserEffects),
    BrowserAnimationsModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  providers: [WordpressService, CacheService, CookieService, PageResolver],
  entryComponents: [SimpleTextComponent, ImageGridComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
