import { SimpleTextComponent } from './simple-text/simple-text.component';
import { ImageGridComponent } from './image-grid/image-grid.component';

export const PageComponents = [
    SimpleTextComponent,
    ImageGridComponent
];