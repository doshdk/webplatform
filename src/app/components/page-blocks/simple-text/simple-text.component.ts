import { Component, OnInit, Input } from '@angular/core';

import { Store } from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import {Observable} from 'rxjs/observable';

@Component({
  selector: 'app-simple-text',
  templateUrl: './simple-text.component.html',
  styleUrls: ['./simple-text.component.scss']
})
export class SimpleTextComponent implements OnInit {
  static ref = 'simple_text';
  @Input() data: any;
  localData: any;
  content: any;
  isLoggedIn: Observable<boolean>;
  isEditing = false;
  froalaOptions = {
    imageManagerLoadURL: 'http://webbuilder.dev/wp-json/dosh/v1/images',
    imageUploadURL: 'http://webbuilder.dev/wp-json/dosh/v1/upload'
  };

  constructor(private store: Store<fromRoot.State>) {
    this.isLoggedIn = this.store.select(fromRoot.getLoggedInStatus);
  }

  ngOnInit() {
    this.localData = this.data;
    this.content = this.data;
  }

  activateEdit(event) {
    this.isEditing = true;
    event.stopPropagation();
  }

  cancelEdit(event) {
    this.isEditing = false;
    event.stopPropagation();
  }

}
