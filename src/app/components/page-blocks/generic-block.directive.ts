import { Directive, ViewContainerRef, ComponentFactoryResolver, Input, OnChanges } from '@angular/core';

import {PageComponents} from './blocks.module';

@Directive({
    selector: '[generic-block]'
})
export class GenericBlockDirective implements OnChanges{

    @Input()
    public data;

    constructor(
        private _vcRef: ViewContainerRef,
        private _cfResolver: ComponentFactoryResolver
    ) { }

    ngOnChanges(changes) {
        if(this.data) {
            this._vcRef.clear();
            const cf = this._cfResolver.resolveComponentFactory(PageComponents.find(component => component.ref === this.data.acf_fc_layout));
            const _component = this._vcRef.createComponent(cf);
            _component.instance['data'] = this.data;
        }
    }
}