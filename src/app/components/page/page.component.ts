import {Component, OnDestroy, OnInit} from '@angular/core';
import { Store } from '@ngrx/store';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';

import * as fromRoot from '../../reducers';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  animations: [
    trigger('visibilityChanged', [
    state('shown' , style({ opacity: 1 })),
    state('hidden', style({ opacity: 0 })),
    transition('* => *', animate('.5s ease-out'))
  ])]
})
export class PageComponent implements OnInit, OnDestroy {
  loading = true;
  page: Observable<any>;
  blocks: any;
  animation = 'hidden';

  constructor(store: Store<fromRoot.State>) {
    this.page = store.select(fromRoot.getCurrentPage).map((page) => {
      this.blocks = page.acf.flexible_content;
    });
    store.select(fromRoot.getPageLoadingStatus).map(loading => {
      this.loading = loading;

      if(this.loading) {
        this.animation = 'hidden';
      } else {
        this.animation = 'shown';
      }
    }).subscribe();

  }

  ngOnInit() {

  }

  ngOnDestroy() {
  }

}
