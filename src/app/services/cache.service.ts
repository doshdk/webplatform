import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class CacheService {
  pages: any[] = [];

  constructor() { }

  setPage(slug, page) {
    this.pages[slug] = page;
  }

  getPage(slug) {
    return this.pages[slug];
  }

}
