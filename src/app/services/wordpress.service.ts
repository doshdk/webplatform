import {Injectable} from '@angular/core';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {Store} from '@ngrx/store';
import * as fromRoot from '../reducers';
import * as pageActions from '../actions/page.actions';
import { CacheService } from './cache.service';

@Injectable()
export class WordpressService {
    baseUrl = 'http://webbuilder.dev/wp-json';

    constructor(private http: Http, private store: Store<fromRoot.State>, private cache: CacheService) {
    }

    logIn() {
        return this.http.post(`${this.baseUrl}/jwt-auth/v1/token`, {
            username: 'admin',
            password: '48792590'
        })
            .map((res: Response) => {
                return res.json();
            })
            .catch((error) => Observable.throw(error.json().error));
    }

    checkAuth(token) {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        });
        const options = new RequestOptions({headers: headers});
        return this.http.post(`${this.baseUrl}/jwt-auth/v1/token/validate`, {}, options)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error) => Observable.throw(error.json().error));
    }

    getPageBySlug(slug: string) {
        if (!this.cache.getPage(slug)) {
            this.store.dispatch(new pageActions.LoadPageAction());
            return this.http.get(`${this.baseUrl}/wp/v2/pages/?slug=${slug}`)
                .map((res: Response) => {
                    this.store.dispatch(new pageActions.PageLoadedAction(res.json()[0]));
                    this.cache.setPage(slug, res.json()[0]);
                    return res.json()[0];
                })
                .catch((error) => Observable.throw(error.json().error));
        } else {
            this.store.dispatch(new pageActions.LoadPageAction());
            // Maybe see if we can find a work around to solve animations and instant data
            setTimeout(() => {
                this.store.dispatch(new pageActions.PageLoadedAction(this.cache.getPage(slug)));
                return Observable.of(this.cache.getPage(slug));
            }, 500);
        }

    }


}
