import {Injectable} from '@angular/core';
import {Store, Action} from '@ngrx/store';
import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {WordpressService} from '../services/wordpress.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import * as user from '../actions/user.actions';
import * as fromRoot from '../reducers';


@Injectable()
export class UserEffects{
    constructor(private wordpress: WordpressService,
                private actions$: Actions,
                private cookieService: CookieService,
                private store: Store<fromRoot.State>) {}

    @Effect() logUserIn$ = this.actions$
        .ofType(user.LOG_IN)
        .switchMap(() => (
            this.wordpress.logIn()
                .map((data) => {
                    this.cookieService.put('userinfo', JSON.stringify(data));
                    return {type: user.LOG_IN_SUCCESS, payload: data};
                })
        ));

    @Effect() getLoginStatus$ = this.actions$
        .ofType(user.GET_LOG_IN_STATUS)
        .switchMap(() => {
            const rawInfo = this.cookieService.get('userinfo');
            if (typeof rawInfo !== 'undefined') {
                const userInfo = JSON.parse(rawInfo);

                return this.wordpress.checkAuth(userInfo.token)
                    .map((data) => {
                        return {type: user.AUTH_VALID, payload: userInfo};
                    }).catch((error) => {
                        this.cookieService.remove('userinfo');
                        this.store.dispatch({type: user.AUTH_INVALID});
                        return Observable.throw(error.json().error);
                    });
            } else {
                return Observable.of({type: user.AUTH_INVALID});
            }

        });

    @Effect() logUserOut$ = this.actions$
        .ofType(user.LOG_OUT)
        .switchMap(() => {
            this.cookieService.remove('userinfo');
            return Observable.of({type: user.LOGGED_OUT});
        });
}