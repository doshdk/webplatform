import {Injectable} from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import {WordpressService} from '../services/wordpress.service';

@Injectable()
export class PageResolver implements Resolve<any> {
    constructor(private wordpress: WordpressService) {}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any>|Promise<any>|any {
        return this.wordpress.getPageBySlug(route.params.slug ? route.params.slug : 'forsiden');
    }
}

